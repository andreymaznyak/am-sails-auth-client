/**
 * Created by AndreyMaznyak on 01.09.2016.
 */
'use strict';
'use strict';

function CurrentUserService($sails, localStorageService, $q){
    const self = this;
    self.error = false;
    self.authenticating = false;
    self.user =  { userlink: false };

    if(localStorageService.isSupported) {
        //...
        const auth_data = localStorageService.get('auth');
        if(!!auth_data){
            self.login_data = auth_data;
            $sails.post('/auth/local',self.login_data).then(result_login,error);
            self.authenticating = true;
        }
    }

    self.logout = ()=>{
        $sails.get('/logout').then(function(){
            if(localStorageService.isSupported) {
                //...
                localStorageService.remove('auth');

            }
            self.user.userlink = false;
        }, function(err){
            console.error(err);
        });
    };

    self.login = (login,password)=>{

        if(!self.deffered){
            self.deffered = $q.defer();
        }

        self.authenticating = true;

        self.login_data = {identifier: login, password: password};
        $sails.post('/auth/local',self.login_data).then(result_login,error);

        return self.deffered.promise;
    };

    function result_login(resp){
        let result = {};
        self.authenticating = resp.data.auth;

        if(!resp.data.user){
            console.log('Неверное имя пользователя или пароль');
            result.error = 'Неверное имя пользователя или пароль';
            if(!!self.deffered){
                self.deffered.reject(result);
                delete self.deffered;
            }

        }else{
            result.error = false;
            if(localStorageService.isSupported) {
                //...
                const auth_data = localStorageService.get('auth');
                if(!auth_data) {
                    localStorageService.set('auth', {identifier:self.login_data.identifier, password: self.login_data.password});//md5($scope.login.identifier + ':' + $scope.login.password));
                }
            }else{
                console.log('Локальное хранилище не поддерживается в вашем браузере. Обновите браузер что бы не перелогиниваться при обновлении страницы');
            }
            //config.currentUser = resp.data.user;
            self.user.userlink = resp.data.user;
            result.user = resp.data.user;

            //Передаёт управление в в таскс который обьявлен в он_конфиге
            //TODO: По сути можно хранить дефолтный стейт у юзера и отправлять его туда
            // $state.go('orders');
            if(!!self.deffered) {
                self.deffered.resolve(result);
                delete self.deffered;
            }
        }
        if(!!self.login_data)
            delete self.login_data;

    }

    function error(err){

        if(localStorageService.isSupported) {
            //...
            localStorageService.remove('auth');

        }else{
            console.log('Локальное хранилище не поддерживается в вашем браузере. Обновите браузер что бы не перелогиниваться при обновлении страницы');
        }

        console.log('Ошибка авторизации ', error);

        if(!!self.deffered){
            self.deffered.reject({err:err});
            delete self.deffered;
        }
        if(!!self.login_data)
            delete self.login_data;
    }

}

CurrentUserService.$inject = ['$sails', 'localStorageService', '$q'];


module.exports = {
    CurrentUserServiceDefault:CurrentUserService
};